%global _empty_manifest_terminate_build 0
Name:           python-jaraco-collections
Version:        5.1.0
Release:        1
Summary:        Collection objects similar to those in stdlib by jaraco
License:        MIT
URL:            https://github.com/jaraco/jaraco.collections
Source0:        https://files.pythonhosted.org/packages/8c/ed/3f0ef2bcf765b5a3d58ecad8d825874a3af1e792fa89f89ad79f090a4ccc/jaraco_collections-5.1.0.tar.gz
BuildArch:      noarch

Requires:       python3-sphinx
Requires:       python3-pytest
Requires:       python3-pytest-checkdocs
Requires:       python3-pytest-flake8
Requires:       python3-pytest-cov
Requires:       python3-pytest-enabler
Requires:       python3-pytest-black
Requires:       python3-pytest-mypy

%description
A dictionary-like object that maps a range of values to a given value.

%package -n python3-jaraco-collections
Summary:        Collection objects similar to those in stdlib by jaraco
Provides:       python-jaraco-collections = %{version}-%{release}
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:  python3-pip python3-wheel python3-hatch-vcs
%description -n python3-jaraco-collections
A dictionary-like object that maps a range of values to a given value.

%package help
Summary:        Development documents and examples for jaraco.collections
Provides:       python3-jaraco-collections-doc
%description help
A dictionary-like object that maps a range of values to a given value.

%prep
%autosetup -n jaraco_collections-%{version} -p1

%build
%pyproject_build

%install
%pyproject_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi

%files -n python3-jaraco-collections
%{python3_sitelib}/*

%files help
%{_docdir}/*

%changelog
* Tue Sep 3 2024 lilu <lilu@kylinos.cn> - 5.1.0-1
- Update package to version 5.1.0
  * Fully typed "RangeMap" and avoid complete iterations to find matches

* Wed Aug 07 2024 yuanlipeng <yuanlipeng2@huawei.com> - 5.0.1-1
- Update to 5.0.1
  * Delinting and package refresh.

* Wed Feb 21 2024 jiangxinyu <jiangxinyu@kylinos.cn> - 5.0.0-1
- Update package to version 5.0.0

* Tue Apr 25 2023 yaoxin <yao_xin@hoperun.com> - 4.1.0-2
- Modify the package compilation method and remove the adaptation to setup.py

* Mon Apr 10 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 4.1.0-1
- Update package to version 4.1.0

* Tue Feb 28 2023 yaoxin <yaoxin30@h-partners.com> - 3.8.0-2
- Adaptation to setup.py and modify the patching mode to resolve installation conflicts

* Fri Dec 16 2022 lijian <lijian2@kylinos.cn> - 3.8.0-1
- update to upstream version 3.8.0

* Tue Aug 30 2022 liqiuyu <liqiuyu@kylinos.cn> - 3.5.2-1
- Upgrade to version 3.5.2

* Fri Jun 17 2022 houyingchao <houyingchao@h-partners.com> - 3.5.1-1
- Upgrade to version 3.5.1

* Fri Nov 26 2021 liuqinfei <18138800392@163.com> - 3.4.0-1
- update version to 3.4.0
- recover some excluded files in %files python3-jaraco-collections
  - __init__.py
  - collections.py

* Wed Sep 1 2021 maxim.suen <maxim2010@163.com> - 3.0.0-2
- Add 'exclude python37*' to %file

* Mon Nov 16 2020 Python_Bot <Python_Bot@openeuler.org>
- Package init
